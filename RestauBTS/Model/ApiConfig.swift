//
//  ApiConfig.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 23/12/2019.
//  Copyright © 2019 Daniel silva vieira. All rights reserved.
//

struct ApiConfig {
    static let url: String = "https://restaubts.superbock.fun/api";
}
