//
//  Login.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 11/03/2020.
//  Copyright © 2020 Daniel silva vieira. All rights reserved.
//

struct Login: Decodable {
    let access_token: String;
}
