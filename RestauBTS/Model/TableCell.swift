//
//  TableCell.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 27/12/2019.
//  Copyright © 2019 Daniel silva vieira. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {
    @IBOutlet weak var tableNr: UILabel!
    @IBOutlet weak var tablePlaces: UILabel!
    @IBOutlet weak var tableStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib();
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated);
    }
    
    func setTableValues(number: Int, places: Int, status: String) -> Void {
        tableNr.text = "Table " + String(number);
        tablePlaces.text = String(places) + " Places";
        tableStatus.text = status;
    }
}
