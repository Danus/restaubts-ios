//
//  Products.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 04/01/2020.
//  Copyright © 2020 Daniel silva vieira. All rights reserved.
//

struct Products: Decodable {
    let products: [Product];
}

struct Product: Decodable {
    let idProduct: Int;
    let dtName: String;
    let dtCategory: String;
    let dtValue: Double;
    let dtImage: String;
}
