//
//  Bills.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 06/01/2020.
//  Copyright © 2020 Daniel silva vieira. All rights reserved.
//

struct Bill: Decodable {
    let products: [ProductBill];
}

struct ProductBill: Decodable {
    let idProduct: Int;
    let dtName: String;
    let dtQuantity: Int;
    let dtValue: Double;
    let dtImage: String;
    let id: String;
}
