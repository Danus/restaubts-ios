//
//  Table.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 27/12/2019.
//  Copyright © 2019 Daniel silva vieira. All rights reserved.
//

struct Tables: Decodable {
    let tables: [Table];
}

struct Table: Decodable {
    let idTableNr: Int;
    let dtReservedName: String;
    let dtIsOccupied: Int;
    let dtPlaces: Int;
}

struct TableStatus: Decodable {
    let isOccupied: Int;
}
