//
//  BillCell.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 06/01/2020.
//  Copyright © 2020 Daniel silva vieira. All rights reserved.
//

import UIKit

class BillCell: UITableViewCell {
    
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib();
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated);
    }
    
    func setBillValues(pQuantity: Int, pId: Int, pName: String, pValue: Double, pImg: String) -> Void {
        quantity.text = "Qty: " + String(pQuantity);
        name.text = pName;
        value.text = String(format: "%.2f", pValue)+"€";
        id.text = "#" + String(pId)
        
        let myUrl = URL(string: pImg)!;
        
        DispatchQueue.global().async {
            do {
                let myData = try Data(contentsOf: myUrl)
                // switchback to main queue
                DispatchQueue.main.sync {
                    self.img.image = UIImage(data: myData)
                }
            } catch{
                print("Image not loaded");
            }
        }
    }
}
