//
//  ProductCell.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 04/01/2020.
//  Copyright © 2020 Daniel silva vieira. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
    
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib();
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated);
    }
    
    func setProductValues(pId: Int, pName: String, pValue: Double, pImg: String) -> Void {
        id.text = "#" + String(pId);
        name.text = pName;
        value.text = String(format: "%.2f", pValue)+"€";
        let myUrl = URL(string: pImg)!;
        
        DispatchQueue.global().async {
            do {
                let myData = try Data(contentsOf: myUrl)
                // switchback to main queue
                DispatchQueue.main.sync {
                    self.img.image = UIImage(data: myData)
                }
            } catch{
                print("Image not loaded");
            }
        }
    }
}
