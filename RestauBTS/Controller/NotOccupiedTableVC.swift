//
//  NotOccupiedTableVC.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 02/01/2020.
//  Copyright © 2020 Daniel silva vieira. All rights reserved.
//

import UIKit
import SwiftSpinner

class NotOccupiedTableVC: UIViewController {
    
    var tableNr: Int!
    var reservation: String!
    let defaults = UserDefaults.standard;
    
    @IBAction func ReserveImgBtn(_ sender: Any) {
        newReservation();
    }
    @IBAction func ReserveBtn(_ sender: Any) {
        newReservation();
    }
    @IBAction func CancelRvImgBtn(_ sender: Any) {
        cancelReservation();
    }
    @IBAction func CancelRvBtn(_ sender: Any) {
        cancelReservation();
    }
    @IBAction func OccupiedImgBtn(_ sender: Any) {
        SwiftSpinner.show("Performing request...");
        performRequest(type: "occupied", data: "1");
    }
    @IBAction func OccupiedBtn(_ sender: Any) {
        SwiftSpinner.show("Performing request...");
        performRequest(type: "occupied", data: "1");
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.title = "Table " + String(tableNr);
    }
    
    //refresh table when the VC will be displayed
    override func viewWillAppear(_ animated: Bool) {
        if (!CheckInternet.Connection()){
            noInternetAlert();
        }
    }
    
    func noInternetAlert (){
        let alert = UIAlertController(title: "Not connected to the internet", message: "You must be connected to internet to use this Application", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func newReservation() {
        var textField = UITextField();
        
        //create new alertController
        let alert = UIAlertController(title: "New Reservation", message: "", preferredStyle: .alert);
        
        //create an action for the alertController
        let actionCreate = UIAlertAction(title: "Create", style: .default) { (action) in
            //this code will be executed when the user presses the add action button of the alertController
            if (textField.text! != "") {
                SwiftSpinner.show("Creating reservation...");
                self.performRequest(type: "reservation", data: textField.text!);
            }
        };
        
        //cancel button
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel);
        
        //add textfield to alertController
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Name";
            textField = alertTextField;
        }
        
        //add the aciton to the actionController
        alert.addAction(actionCreate);
        alert.addAction(actionCancel);
        
        //present the alertController on scene
        present(alert, animated: true, completion: nil);
    }
    
    func cancelReservation() {
        if (reservation == "") {
            //create new alertController
            let alert = UIAlertController(title: "Not Reserved", message: "The table is not reserved!", preferredStyle: .alert);
            
            //create an action for the alertController
            let actionYes = UIAlertAction(title: "Ok", style: .default);
            
            //add the aciton to the actionController
            alert.addAction(actionYes);
            
            //present the alertController on scene
            present(alert, animated: true, completion: nil);
        } else {
            //create new alertController
            let alert = UIAlertController(title: "Cancel Reservation", message: "Do you want to cancel this reservation?", preferredStyle: .alert);
            
            //create an action for the alertController
            let actionYes = UIAlertAction(title: "Yes", style: .destructive) { (action) in
                //this code will be executed when the user presses the add action button of the alertController
                SwiftSpinner.show("Canceling reservation...");
                self.performRequest(type: "reservation", data: "");
            };
            
            //cancel button
            let actionNo = UIAlertAction(title: "No", style: .cancel);
            
            //add the aciton to the actionController
            alert.addAction(actionYes);
            alert.addAction(actionNo);
            
            //present the alertController on scene
            present(alert, animated: true, completion: nil);
        }
    }
    
    func performRequest (type: String, data: String) -> Void {
        if let url = URL(string: ApiConfig.url+"/table/changeStatus") {
            var request = URLRequest(url: url)
            
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue(defaults.string(forKey: "token")!, forHTTPHeaderField: "Authorization")
            request.httpMethod = "POST"
            
            let parameters: [String: Any] = [
                "tableNr": tableNr!,
                "type": type,
                "data": data
            ]
            
            request.httpBody = parameters.percentEncoded()
            
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                SwiftSpinner.hide();
                guard let response = response as? HTTPURLResponse,
                error == nil else {
                    //check for fundamental networking error
                    print("error", error ?? "Unknown error");
                    return;
                }

                guard response.statusCode == 201 else {
                    //check for http errors
                    return;
                }
                
                DispatchQueue.main.async {
                    _ = self.navigationController?.popViewController(animated: true);
                }
            }
            
            task.resume();
        }
    }
    
}
