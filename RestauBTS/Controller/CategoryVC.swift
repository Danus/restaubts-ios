//
//  CategoryVC.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 03/01/2020.
//  Copyright © 2020 Daniel silva vieira. All rights reserved.
//

import UIKit
import SwiftSpinner

class CategoryVC: UIViewController {
    var category: String!;
    var dataFromPreviousVC: Int!
    
    @IBAction func appetizers(_ sender: Any) {
        category = "Appetizers";
        performSegue(withIdentifier: "products", sender: self);
    }
    @IBAction func mainDishes(_ sender: Any) {
        category = "Main Dishes";
        performSegue(withIdentifier: "products", sender: self);
    }
    @IBAction func menus(_ sender: Any) {
        category = "Menus";
        performSegue(withIdentifier: "products", sender: self);
    }
    @IBAction func desserts(_ sender: Any) {
        category = "Desserts";
        performSegue(withIdentifier: "products", sender: self);
    }
    @IBAction func coldDrinks(_ sender: Any) {
        category = "Cold Drinks";
        performSegue(withIdentifier: "products", sender: self);
    }
    @IBAction func warmDrinks(_ sender: Any) {
        category = "Hot Drinks";
        performSegue(withIdentifier: "products", sender: self);
    }
    @IBAction func bill(_ sender: Any) {
        category = "Bill";
        performSegue(withIdentifier: "bill", sender: self);
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let productVC = segue.destination as? ProductVC {
            productVC.category = category;
            productVC.tableNr = String(dataFromPreviousVC);
        }
        if let billVC = segue.destination as? BillVC {
            billVC.tableNr = String(dataFromPreviousVC);
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.title = "Table " + String(dataFromPreviousVC);
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (!CheckInternet.Connection()){
            noInternetAlert();
        } else {
            SwiftSpinner.show("Checking Table Status...");
            checkTableStatus();
        }
    }
    
    func checkTableStatus() -> Void {
        if let url = URL(string: ApiConfig.url + "/table/checkStatus/" + String(dataFromPreviousVC)) {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error!);
                    return
                }
                if let safeData = data {
                    self.parseJson(jsonData: safeData)
                }
            }
            task.resume();
        }
    }
    func parseJson (jsonData: Data) {
        let decoder = JSONDecoder();
        do {
            let decodedData = try decoder.decode(TableStatus.self, from: jsonData);
            DispatchQueue.main.async {
                SwiftSpinner.hide()
                if (decodedData.isOccupied == 0) {
                    _ = self.navigationController?.popViewController(animated: true);
                }
            }
        } catch {
            print(error);
        }
    }
    
    func noInternetAlert (){
        let alert = UIAlertController(title: "Not connected to the internet", message: "You must be connected to internet to use this Application", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
