//
//  ProductsVC.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 04/01/2020.
//  Copyright © 2020 Daniel silva vieira. All rights reserved.
//

import UIKit
import SwiftSpinner

class ProductVC: UITableViewController {

    var productArray = [Product]();
    var dataToSend: String! = "";
    var category: String!;
    var tableNr: String!;
    let defaults = UserDefaults.standard;

    override func viewDidLoad() {
        super.viewDidLoad();
        self.title = category;
    }
    
    //refresh table when the VC will be displayed
    override func viewWillAppear(_ animated: Bool) {
        if (!CheckInternet.Connection()){
            noInternetAlert();
        } else {
            SwiftSpinner.show("Loading Products...");
            loadProducts(search: "");
        }
    }
    
    func noInternetAlert (){
        let alert = UIAlertController(title: "Not connected to the internet", message: "You must be connected to internet to use this Application", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func loadProducts(search: String) -> Void {
        if let url = URL(string: ApiConfig.url+"/product/getByCategory/"+category.replacingOccurrences(of: " ", with: "%20")+"/"+search.replacingOccurrences(of: " ", with: "%20")) {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error!);
                    return
                }
                if let safeData = data {
                    self.parseJson(jsonData: safeData)
                }
            }
            task.resume();
        }
    }
    func parseJson (jsonData: Data) {
        let decoder = JSONDecoder();
        do {
            let decodedData = try decoder.decode(Products.self, from: jsonData);
            self.productArray = decodedData.products
            DispatchQueue.main.async {
                self.tableView.reloadData();
            }
            SwiftSpinner.hide();
        } catch {
            print(error);
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productArray.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath) as? ProductCell {
            
            cell.setProductValues(
                pId: productArray[indexPath.row].idProduct,
                pName: productArray[indexPath.row].dtName,
                pValue: productArray[indexPath.row].dtValue,
                pImg: productArray[indexPath.row].dtImage
            );
            
            return cell
        } else{
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        addProduct(product: productArray[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true);
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90;
    }
    
    func addProduct(product: Product) {
        var textField = UITextField();
        
        //create new alertController
        let alert = UIAlertController(title: "Add Product: "+product.dtName, message: "Quantity:", preferredStyle: .alert);
        
        //create an action for the alertController
        let actionAdd = UIAlertAction(title: "Add", style: .default) { (action) in
            //this code will be executed when the user presses the add action button of the alertController
            if (textField.text! != "" && Int(textField.text!) != nil) {
                SwiftSpinner.show("Adding Product...");
                self.performAddProductRequest(productId: product.idProduct, quantity: textField.text!);
            } else {
                let errorAlert = UIAlertController(title: "ERROR", message: "The Product ("+product.dtName+") has not been added. Please check your Input!", preferredStyle: .alert);
                let actionOk = UIAlertAction(title: "OK", style: .default);
                errorAlert.addAction(actionOk);
                self.present(errorAlert, animated: true, completion: nil);
            }
        };
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel);
        
        //add textfield to alertController
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Quantity";
            alertTextField.text = "1";
            textField = alertTextField;
        }
        
        //add the aciton to the actionController
        alert.addAction(actionAdd);
        alert.addAction(actionCancel);
        
        //present the alertController on scene
        present(alert, animated: true, completion: nil);
    }
    
    func performAddProductRequest (productId: Int, quantity: String) -> Void {
        if let url = URL(string: ApiConfig.url+"/bill/addProduct") {
            var request = URLRequest(url: url)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue(defaults.string(forKey: "token")!, forHTTPHeaderField: "Authorization")
            request.httpMethod = "POST"
            
            let parameters: [String: Any] = [
                "tableNr": tableNr!,
                "productId": productId,
                "quantity": quantity
            ]
            
            request.httpBody = parameters.percentEncoded()
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                SwiftSpinner.hide();
                
                guard let response = response as? HTTPURLResponse,
                error == nil else {
                    //check for fundamental networking error
                    print("error", error ?? "Unknown error");
                    return;
                }

                guard response.statusCode == 201 else {
                    //check for http errors
                    return;
                }
            }
            
            task.resume();
        }
    }
}
extension ProductVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchBar.text?.count == 0) {
            SwiftSpinner.show("Loading Products...");
            loadProducts(search: "");
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        SwiftSpinner.show("Searching...");
        loadProducts(search: searchBar.text!);
    }
}
