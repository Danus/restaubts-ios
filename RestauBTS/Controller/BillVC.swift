//
//  BillVC.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 06/01/2020.
//  Copyright © 2020 Daniel silva vieira. All rights reserved.
//

import UIKit
import SwiftSpinner

class BillVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var billArray = [ProductBill]();
    var tableNr: String!;
    let defaults = UserDefaults.standard;
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalLabel: UILabel!
    
    @IBAction func finishServiceBtn(_ sender: Any) {
        finishService();
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.title = "Bill";
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    //refresh table when the VC will be displayed
    override func viewWillAppear(_ animated: Bool) {
        if (!CheckInternet.Connection()){
            noInternetAlert();
        } else {
            SwiftSpinner.show("Loading Bill...");
            loadBillProducts(search: "");
        }
    }
    
    func noInternetAlert (){
        let alert = UIAlertController(title: "Not connected to the internet", message: "You must be connected to internet to use this Application", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return billArray.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "billCell", for: indexPath) as? BillCell {
            
            cell.setBillValues(
                pQuantity: billArray[indexPath.row].dtQuantity,
                pId: billArray[indexPath.row].idProduct,
                pName: billArray[indexPath.row].dtName,
                pValue: billArray[indexPath.row].dtValue,
                pImg: billArray[indexPath.row].dtImage
            );
            
            return cell
        } else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        modifyBillProduct(product: billArray[indexPath.row]);
        tableView.deselectRow(at: indexPath, animated: true);
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
      if editingStyle == .delete {
        
        let verficationAlert = UIAlertController(title: "Delete", message: "Do you want to delete the Product ("+self.billArray[indexPath.row].dtName+")?", preferredStyle: .alert);
        
        let actionYes = UIAlertAction(title: "Yes", style: .destructive){ (action) in
            SwiftSpinner.show("Deleting...")
            self.deleteBillProductRequest(productId: self.billArray[indexPath.row].idProduct, id: self.billArray[indexPath.row].id);
        }
        
        let actionNo = UIAlertAction(title: "No", style: .default);
        
        verficationAlert.addAction(actionNo);
        verficationAlert.addAction(actionYes);
        self.present(verficationAlert, animated: true, completion: nil);
      }
    }
    
    //load table content
    func loadBillProducts(search: String) -> Void {
        if let url = URL(string: ApiConfig.url+"/product/getByTable/"+tableNr+"/"+search.replacingOccurrences(of: " ", with: "%20")) {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error!);
                    return
                }
                if let safeData = data {
                    self.parseJson(jsonData: safeData)
                }
            }
            task.resume();
        }
    }
    func parseJson (jsonData: Data) {
        let decoder = JSONDecoder();
        do {
            let decodedData = try decoder.decode(Bill.self, from: jsonData);
            self.billArray = decodedData.products
                
            var total: Double = 0;

            billArray.forEach { product in
                total += product.dtValue * Double(product.dtQuantity)
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData();
                self.totalLabel.text = "Total: "+String(format: "%.2f", total)+"€";
            }
            
            SwiftSpinner.hide();
        } catch {
            print(error);
        }
    }
    
    
    // MARK: Actions
    
    func finishService() {
        //create new alertController
        let alert = UIAlertController(title: "Finish Service", message: "Do you want finish the service? After this you will not be able to modify this bill!", preferredStyle: .alert);
        
        //create an action for the alertController
        let actionYes = UIAlertAction(title: "Yes", style: .destructive) { (action) in
            //this code will be executed when the user presses the add action button of the alertController
            SwiftSpinner.show("Finishing Service...");
            self.finishServiceRequest();
        };
        
        //cancel button
        let actionNo = UIAlertAction(title: "No", style: .cancel);
        
        //add the aciton to the actionController
        alert.addAction(actionYes);
        alert.addAction(actionNo);
        
        //present the alertController on scene
        present(alert, animated: true, completion: nil);
    }
    
    func modifyBillProduct(product: ProductBill) {
        var textField = UITextField();
        
        //create new alertController
        let alert = UIAlertController(title: "Modify quantity of the product: "+product.dtName, message: "Quantity:", preferredStyle: .alert);
        
        //create an action for the alertController
        let actionModify = UIAlertAction(title: "Modify", style: .default) { (action) in
            //this code will be executed when the user presses the add action button of the alertController
            if (textField.text! != "" && Int(textField.text!) != nil) {
                SwiftSpinner.show("Modifying...")
                self.modifyBillProductRequest(productId: product.idProduct, id: product.id, quantity: Int(textField.text!) ?? 0);
            } else {
                let errorAlert = UIAlertController(title: "ERROR", message: "The Product ("+product.dtName+") has not been modified. Please check your Input!", preferredStyle: .alert);
                let actionOk = UIAlertAction(title: "OK", style: .default);
                errorAlert.addAction(actionOk);
                self.present(errorAlert, animated: true, completion: nil);
            }
        };
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel);
        
        //add textfield to alertController
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Quantity";
            alertTextField.text = String(product.dtQuantity);
            textField = alertTextField;
        }
        
        //add the aciton to the actionController
        alert.addAction(actionModify);
        alert.addAction(actionCancel);
        
        //present the alertController on scene
        present(alert, animated: true, completion: nil);
    }
    
    
    // MARK: Requests
    
    func finishServiceRequest() -> Void {
        if let url = URL(string: ApiConfig.url+"/table/changeStatus") {
            var request = URLRequest(url: url)
            
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue(defaults.string(forKey: "token")!, forHTTPHeaderField: "Authorization")
            request.httpMethod = "POST"
            
            let parameters: [String: Any] = [
                "tableNr": tableNr!,
                "type": "serviceFinished",
                "data": ""
            ]
            
            request.httpBody = parameters.percentEncoded()
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                SwiftSpinner.hide();
                guard let response = response as? HTTPURLResponse,
                error == nil else {
                    //check for fundamental networking error
                    print("error", error ?? "Unknown error");
                    return;
                }

                guard response.statusCode == 201 else {
                    //check for http errors
                    return;
                }

                DispatchQueue.main.async {
                    _ = self.navigationController?.popViewController(animated: true);
                }
            }
            
            task.resume();            
        }
    }
    
    func deleteBillProductRequest (productId: Int, id: String) -> Void {
        if let url = URL(string: ApiConfig.url+"/bill/deleteProduct") {
            var request = URLRequest(url: url)
            
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue(defaults.string(forKey: "token")!, forHTTPHeaderField: "Authorization")
            request.httpMethod = "POST"
            
            let parameters: [String: Any] = [
                "tableNr": tableNr!,
                "productId": productId,
                "id": id
            ]
            
            request.httpBody = parameters.percentEncoded()
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                
                guard let response = response as? HTTPURLResponse,
                error == nil else {
                    //check for fundamental networking error
                    print("error", error ?? "Unknown error");
                    return;
                }

                guard response.statusCode == 201 else {
                    //check for http errors
                    return;
                }
                self.loadBillProducts(search: "");
            }
            
            task.resume();
        }
    }
    
    func modifyBillProductRequest (productId: Int, id: String, quantity: Int) -> Void {
        if let url = URL(string: ApiConfig.url+"/bill/editProduct") {
            var request = URLRequest(url: url)
            
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue(defaults.string(forKey: "token")!, forHTTPHeaderField: "Authorization")
            request.httpMethod = "POST"
            
            let parameters: [String: Any] = [
                "tableNr": tableNr!,
                "productId": productId,
                "id": id,
                "quantity": quantity
            ]
            
            request.httpBody = parameters.percentEncoded()
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                
                guard let response = response as? HTTPURLResponse,
                error == nil else {
                    //check for fundamental networking error
                    print("error", error ?? "Unknown error");
                    return;
                }

                guard response.statusCode == 201 else {
                    //check for http errors
                    return;
                }
                self.loadBillProducts(search: "");
            }
            
            task.resume();
        }
    }
    
}
extension BillVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchBar.text?.count == 0) {
            SwiftSpinner.show("Loading Bill...");
            loadBillProducts(search: "");
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        SwiftSpinner.show("Searching...");
        loadBillProducts(search: searchBar.text!);
    }
}
