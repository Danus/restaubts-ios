//
//  TableVC.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 27/12/2019.
//  Copyright © 2019 Daniel silva vieira. All rights reserved.
//

import UIKit
import SwiftSpinner

class TableVC: UITableViewController {

    var tableArray = [Table]();
    var dataToSend: Int! = 0;
    var reservationToSend: String = "";
    var allTables: Bool = false;
    let defaults = UserDefaults.standard;
    var oldTableCellOccupied: Int = 0;
    var oldTableCellReservation: String = "";
    
    @IBAction func navButton(_ sender: UIButton) {
        if (allTables) {
            sender.setTitle("My Tables", for: .normal);
            SwiftSpinner.show("Loading Tables...");
            loadTables();
        } else {
            sender.setTitle("All Tables", for: .normal);
            SwiftSpinner.show("Loading Tables...");
            loadTables(user: defaults.string(forKey: "userName")!);
        }
        allTables = !allTables
    }

    override func viewDidLoad() {
        super.viewDidLoad();
        self.title = "RestauBTS Tables";

        //change backButtonTitle
        navigationItem.backBarButtonItem = UIBarButtonItem(
        title: "Back", style: .plain, target: nil, action: nil)
    }
    
    //refresh table when the VC will be displayed
    override func viewWillAppear(_ animated: Bool) {
        if (!CheckInternet.Connection()){
            noInternetAlert();
        } else {
            SwiftSpinner.show("Loading Tables...");
            if (!allTables) {
                loadTables();
            } else {
                loadTables(user: defaults.string(forKey: "userName")!);
            }
        }
    }
    
    func noInternetAlert (){
        let alert = UIAlertController(title: "Not connected to the internet", message: "You must be connected to internet to use this Application", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //prepare data to send (for the next VC's)
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let notOccupiedTableVC = segue.destination as? NotOccupiedTableVC {
            notOccupiedTableVC.tableNr = dataToSend;
            notOccupiedTableVC.reservation = reservationToSend;
        }
        if let categoryVC = segue.destination as? CategoryVC {
            categoryVC.dataFromPreviousVC = dataToSend;
        }
    }
    
    
    //get table content from API
    func loadTables(user: String? = "", indexPathRow: Int? = -1) -> Void {
        var apiUrl: String;
        if (user == "") {
            apiUrl = ApiConfig.url+"/table/getAll";
        } else {
            apiUrl = ApiConfig.url+"/table/getAll/"+user!;
        }
        if let url = URL(string: apiUrl) {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    print(error!);
                    return
                }
                if let safeData = data {
                    self.parseJson(jsonData: safeData, indexPathRow: indexPathRow!)
                }
            }
            task.resume();
        }
    }
    func parseJson (jsonData: Data, indexPathRow: Int) {
        let decoder = JSONDecoder();
        do {
            let decodedData = try decoder.decode(Tables.self, from: jsonData);
            self.tableArray = decodedData.tables
            DispatchQueue.main.async {
                self.tableView.reloadData();
                if (indexPathRow >= 0) {
                    self.checkTableContent(indexPathRow: indexPathRow);
                }
            }
            SwiftSpinner.hide();
        } catch {
            print(error);
        }
    }
    
    func checkTableContent (indexPathRow: Int) -> Void {
        dataToSend = tableArray[indexPathRow].idTableNr;
        reservationToSend = tableArray[indexPathRow].dtReservedName;
        //send to different VC's depending on the status
        
        if (oldTableCellOccupied != tableArray[indexPathRow].dtIsOccupied ||
            oldTableCellReservation != tableArray[indexPathRow].dtReservedName) {
            
            var previousData: String = "";
            
            if (oldTableCellOccupied == 1) {
                previousData = "from occupied";
            } else if (oldTableCellReservation != "") {
                previousData = "from Rv. "+oldTableCellReservation;
            }
            
            var currentData: String = "";
            
            if (tableArray[indexPathRow].dtIsOccupied == 1) {
                currentData = "Occupied";
            } else if (tableArray[indexPathRow].dtReservedName != "") {
                currentData = "Rv. "+tableArray[indexPathRow].dtReservedName;
            } else {
                currentData = "the default status"
            }
            
            let alert = UIAlertController(title: "Changes detected!", message: "The Table status has been changed "+previousData+" to "+currentData+"!", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            if (tableArray[indexPathRow].dtIsOccupied == 1) {
                performSegue(withIdentifier: "occupied", sender: self);
            } else {
                performSegue(withIdentifier: "notOccupied", sender: self);
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableArray.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as? TableCell {
            
            //change status text
            var status: String = "";
            
            if (tableArray[indexPath.row].dtIsOccupied == 1) {
                status = "Occupied";
            } else if (tableArray[indexPath.row].dtReservedName != "") {
                status = "Rv. "+tableArray[indexPath.row].dtReservedName;
            }
            
            //set table cell content
            cell.setTableValues(
                number: tableArray[indexPath.row].idTableNr,
                places: tableArray[indexPath.row].dtPlaces,
                status: status
            );
            
            return cell
        } else{
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SwiftSpinner.show("Loading Tables...");
        oldTableCellOccupied = tableArray[indexPath.row].dtIsOccupied;
        oldTableCellReservation = tableArray[indexPath.row].dtReservedName;
        
        if (!allTables) {
            loadTables(indexPathRow: indexPath.row);
        } else {
            loadTables(user: defaults.string(forKey: "userName")!, indexPathRow: indexPath.row);
        }
        
        tableView.deselectRow(at: indexPath, animated: true);
    }
}
