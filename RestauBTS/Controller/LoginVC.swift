//
//  ViewController.swift
//  RestauBTS
//
//  Created by Daniel silva vieira on 23/12/2019.
//  Copyright © 2019 Daniel silva vieira. All rights reserved.
//

import UIKit
import SwiftSpinner

class LoginVC: UIViewController, UITextFieldDelegate {
    
    let defaults = UserDefaults.standard;
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var errorMessage: UILabel!
    @IBAction func loginBtn(_ sender: UIButton) {
        SwiftSpinner.show("Authentificating User Account");
        performLoginRequest();
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.title = "Login";
        errorMessage.isHidden = true;
        
        username.delegate = self;
        password.delegate = self;
        
        //change backButtonTitle
        navigationItem.backBarButtonItem = UIBarButtonItem(
        title: "Logout", style: .plain, target: nil, action: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        if (!CheckInternet.Connection()){
            noInternetAlert();
        }
    }
    
    func noInternetAlert (){
        let alert = UIAlertController(title: "Not connected to the internet", message: "You must be connected to internet to use this Application", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //when the user presses submit in his keyboard while typing, perform login request or go to next textfield
        if (textField == self.username) {
            self.password.becomeFirstResponder()
        } else {
            SwiftSpinner.show("Authentificating User Account");
            performLoginRequest();
        }
        return true
    }
    
    func performLoginRequest () -> Void {
        self.username.becomeFirstResponder()
        if let url = URL(string: ApiConfig.url+"/user/login") {
            var request = URLRequest(url: url)
            
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
            //post parameters for the api
            let parameters: [String: Any] = [
                "username": username.text!,
                "password": password.text!
            ]
            
            request.httpBody = parameters.percentEncoded()
            
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let response = response as? HTTPURLResponse,
                error == nil else {
                    //check for fundamental networking error
                    print("error", error ?? "Unknown error");
                    SwiftSpinner.hide();
                    return;
                }
                
                SwiftSpinner.hide();

                guard (response.statusCode == 200 || response.statusCode == 201) else {
                    //check for http errors
                    DispatchQueue.main.async {
                        self.errorMessage.isHidden = false;
                    }
                    return;
                }
                
                if let safeData = data {
                    self.parseJson(jsonData: safeData)
                }
            }
            task.resume();
        }
    }
    
    func parseJson (jsonData: Data) {
        let decoder = JSONDecoder();
        do {
            let decodedData = try decoder.decode(Login.self, from: jsonData);
            DispatchQueue.main.async {
                self.defaults.set("Bearer " + decodedData.access_token, forKey: "token")
                self.defaults.set(self.username.text!, forKey: "userName");
                self.errorMessage.isHidden = true;
                self.username.text = "";
                self.password.text = "";
                self.performSegue(withIdentifier: "loginSuccessfull", sender: self);
            }
        } catch {
            print(error);
        }
    }

}

//this code has been taken from this URL: https://stackoverflow.com/questions/26364914/http-request-in-swift-with-post-method !!!

extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}		

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="

        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
